import React from 'react';
import { Provider, connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';
import store from './src/store';
import RootNavigator from './src/navigation/RootNavigation';

class App extends React.Component {
  render() {
    return (
      <RootNavigator navigation={addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: this.props.nav,
      })}/>
    )
  }
}

const mapStateToProps = state => {
  return {
    nav: state.nav
  }
}

const AppWithNavigationState = connect(mapStateToProps)(App);

export default class Root extends React.Component {

  render(){
    return(
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    )
  }

}
