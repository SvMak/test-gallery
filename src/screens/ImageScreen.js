import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';

class SingleImageScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: navigation.state.params.name
  });

  render() {
    const { params } = this.props.navigation.state;
    let { width, height } = Dimensions.get('window');

    return (
      <View style={styles.container}>
        <Image
          style={{
            width: width,
            height: height,
          }}
          source={{uri: params.image_url}}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

function mapStateToProps(state) {
  return {
    images: state.images,
  }
}

export default connect(mapStateToProps)(SingleImageScreen);
