import React from 'react';
import { 
    StyleSheet,
    View,
    Text } from 'react-native';
import { Icon, Button } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

class Item extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={styles.item}>
               <Icon name={this.props.data.icon} />
               <Text style={styles.itemText}>{ this.props.data.about }</Text>
            </View>
        )
    }
}

export default class AboutImageScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        tabBarLabel: 'About'
    });

    resetAction = NavigationActions.reset({
        index: 0,
        actions: [
            NavigationActions.navigate({routeName: 'Home'})
        ]
    })

    render() {
        const { params } = this.props.navigation.state;

        return (
            <View style={styles.container}>  
               <Item data={{about: params.about.camera, icon: 'camera-alt'}}/>

               <Item data={{about: params.about.created_at, icon: 'create'}}/>

               <Item data={{about: params.about.user.country, icon: 'location-on'}}/>

               <Button icon={{name: 'home'}} title='Home' onPress={() => this.props.navigation.dispatch(this.resetAction)}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10
    },
    itemText: {
        marginLeft: 10
    }
});