import React from 'react';
import {
  StyleSheet,
   Text,
   View,
   FlatList,
   ActivityIndicator,
   ImageBackground, Dimensions,
   TouchableHighlight } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getFetchingFromApi } from '../actions';

const queryParams = '&image_size=600,21';



class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'Gallery'
  });

  componentDidMount() {
    this.props.getFetchingFromApi(queryParams);
  }

  _keyExtractor = (item, index) => item.id;

  _renderItem = (data) => {
    const { navigate } = this.props.navigation;

    return (
        <View>
          <TouchableHighlight onPress={() => navigate('Image', {
              image_url: data.item.images[0].url,
              name: data.item.name,
              about: data.item
            }
          )}>
            <View>
              <ImageBackground
                style={styles.itemImg}
                source={{uri: data.item.images[1].url}}>
                <View style={styles.textImg}>
                  <Text style={styles.textColor}>{data.item.name}</Text>
                  <Text style={styles.textColor}>{data.item.user.fullname}</Text>
                </View>
              </ImageBackground>
            </View>
          </TouchableHighlight>
        </View>
    )
  }

  _handleRefresh = () => {
    this.props.getFetchingFromApi(queryParams);
  }

  _handleNextPage = () => {
    this.props.getFetchingFromApi(queryParams, 'next');
  }

  render() {
    const { images, isFetching } = this.props.images;

    if (isFetching) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size={'large'} />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <FlatList
          style={styles.ItemList}
          numColumns={2}
          data={images}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
          onRefresh={this._handleRefresh}
          refreshing={false}
          onEndReached={this._handleNextPage}
          onEndReachedThreshold={1/1000}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ItemList: {
    flex: 1,
  },
  itemImg: {
    margin: 1,
    width: (Dimensions.get('window').width/2)-2,
    height: (Dimensions.get('window').height/3)-29,
  },
  textImg: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  textColor: {
    color: '#ffffff',
  }
});

const mapStateToProps = state => {
  return {
    images: state.images
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({getFetchingFromApi}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
