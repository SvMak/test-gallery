import { TabNavigator } from 'react-navigation';
import ImageScreen from '../screens/ImageScreen';
import AboutImageScreen from '../screens/AboutImageScreen';

const ImageTabNavigator = TabNavigator({
    Image: {
      screen: ImageScreen
    },
    AboutImage: {
      screen: AboutImageScreen
    }
});

export default ImageTabNavigator;