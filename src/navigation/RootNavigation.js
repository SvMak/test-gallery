import { StackNavigator } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import ImageTabNavigator from './ImageTabNavigation';

const RootNavigator = StackNavigator({
    Home: {
        screen: HomeScreen
    },
    Image: {
        screen: ImageTabNavigator
    },
});

export default RootNavigator;