import { NavigationActions } from 'react-navigation';
import RootNavigator from '../navigation/RootNavigation';

const initialState = RootNavigator.router.getStateForAction(NavigationActions.init());

export default function navReducer(state = initialState, action) {
    const nextState = RootNavigator.router.getStateForAction(action, state);

    return nextState || state;
}