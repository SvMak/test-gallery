import { combineReducers } from 'redux';
import imagesReduser from './images';
import navReducer from './nav';

const rootReducer = combineReducers({
  images: imagesReduser,
  nav: navReducer,
});

export default rootReducer;
