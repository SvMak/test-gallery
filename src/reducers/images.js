import { FETCHING_IMAGES, FETCHING_IMAGES_SUCCESS, FETCHING_IMAGES_FIRST_PAGE, FETCHING_IMAGES_NEXT_PAGE, FETCHING_IMAGES_FAILURE } from '../actions';

const initialState = {
  images: [],
  isFetching: false,
  page: 1,
  error: {
    status: false,
    msg: ''
  }
}

export default function imagesReduser(state = initialState, action) {
  switch (action.type) {
    case FETCHING_IMAGES:
      return {
        ...state,
        images: [],
        isFetching: true,
      };
    case FETCHING_IMAGES_FIRST_PAGE:
      return {
        ...state,
        images: [],
        isFetching: false,
        page: 1,
      };
    case FETCHING_IMAGES_NEXT_PAGE:
      return {
        ...state,
        images: [],
        isFetching: false,
        page: state.page + action.amount,
      };
    case FETCHING_IMAGES_SUCCESS:
      return {
        ...state,
        images: action.data,
        isFetching: false,
      };
    case FETCHING_IMAGES_FAILURE:
      return {
        ...state,
        error: {
          status: true,
          msg: action.err,
        }
      };
    default:
      return state;
  }
}
