import { API } from './config';

// API
const api = `${API.url}&${API.consumerKey}`; 

// Fetching data
export const FETCHING_IMAGES_FIRST_PAGE = 'FETCHING_IMAGES_FIRST_PAGE';
export const FETCHING_IMAGES_NEXT_PAGE = 'FETCHING_IMAGES_NEXT_PAGE';

export const FETCHING_IMAGES = 'FETCHING_IMAGES';
export const FETCHING_IMAGES_SUCCESS = 'FETCHING_IMAGES_SUCCESS';
export const FETCHING_IMAGES_FAILURE = 'FETCHING_IMAGES_FAILURE';

export function getFetchingFromApi(queryParams, token) {
  let url = normalizeUrl(api, queryParams);
  let page;

  return (dispatch, getState) => {

    switch (token) {
      case 'next':
        dispatch(getImageNextPage(1));
        page = getState().images.page;
        getFetch(dispatch, `${url}&page=${page}`);

        break;
      default:
        dispatch(getImageFirstPage());
        page = getState().images.page;
        getFetch(dispatch, `${url}&page=${page}`);
    }

  };
}

// Normileze url
function normalizeUrl(api, queryParams) {
  if (queryParams !== '' || queryParams !== undefined) {
    return api+queryParams;
  } else {
    return api;
  }
}

function getFetch(dispatch, url) {
  dispatch(getImage());
  fetch(url)
  .then(res => res.json())
  .then(json => dispatch(getImageSuccess(json.photos)))
  .catch(err => dispatch(getImageFailure(err)));
}

function getImage(page) {
  return {
    type: FETCHING_IMAGES,
    page,
  };
}

function getImageSuccess(data) {
  return {
    type: FETCHING_IMAGES_SUCCESS,
    data,
  };
}

function getImageFailure(err) {
  return {
    type: FETCHING_IMAGES_FAILURE,
    err,
  };
}

function getImageFirstPage() {
  return {
    type: FETCHING_IMAGES_FIRST_PAGE,
  };
}

function getImageNextPage(amount) {
  return {
    type: FETCHING_IMAGES_NEXT_PAGE,
    amount,
  };
}
